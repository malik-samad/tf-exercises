import tensorflow as tf
from tensorflow.keras import layers, losses, optimizers
import matplotlib.pyplot as plt
from tensorflow.keras.preprocessing import image_dataset_from_directory


# visualize model history
def plot_history(history):
    loss = history['loss']
    val_loss = history['val_loss']
    epochs = range(len(loss))

    accuracy = history['accuracy']
    val_accuracy = history['val_accuracy']

    plt.figure(figsize=(5, 3))
    plt.title("Loss curve")
    plt.plot(epochs, loss, label="loss")
    plt.plot(epochs, val_loss, label='val_loss')

    plt.figure(figsize=(5, 3))
    plt.title("Accuracy curve")
    plt.plot(epochs, accuracy, label="accuracy")
    plt.plot(epochs, val_accuracy, label='val_accuracy')
    plt.show()

# 2. Train a feature-extraction transfer learning model for 10 epochs on the same data and
#   compare its performance versus a model which used feature extraction for 5 epochs and fine-tuning
#   for 5 epochs (like we've used in this notebook). Which method is better?

# augmentation layer
augmentation_layer = tf.keras.Sequential([
    layers.experimental.preprocessing.RandomRotation(0.2),
    layers.experimental.preprocessing.RandomZoom(0.2),
    layers.experimental.preprocessing.RandomFlip('horizontal'),
], name="augmentation_layer")


# Load in data
train_dir = "Data/10_food_classes_10_percent/train"
test_dir = "Data/10_food_classes_10_percent/test"

train_data = image_dataset_from_directory(train_dir,
                                          label_mode="categorical",
                                          batch_size=32,
                                          image_size=[224, 224],
                                          shuffle=True)

test_data = image_dataset_from_directory(test_dir,
                                         label_mode="categorical",
                                         batch_size=32,
                                         image_size=[224, 224],
                                         shuffle=True)

#       model-time-series: feature extraction model 10 epochs
base_model = tf.keras.applications.EfficientNetB0(include_top=False)
base_model.trainable = False   # is feature extraction layer only, tuning disabled

inputs = layers.Input([224, 224, 3], name="input_layer")
x = augmentation_layer(inputs)
x = base_model(x)
x = layers.GlobalAveragePooling2D(name="global_avg_pooling")(x)
output = layers.Dense(10, name="output", activation="softmax")(x)

model_1 = tf.keras.Model(inputs, output)
model_1.compile(loss=losses.CategoricalCrossentropy(),
                optimizer=optimizers.Adam(),
                metrics=["accuracy"])

print(model_1.summary())
model_1_history = model_1.fit(train_data,
                              epochs=2,
                              steps_per_epoch=int(len(train_data) * 0.1),
                              validation_data=test_data,
                              validation_steps=int(len(test_data) * 0.1),
                              callbacks=[tf.keras.callbacks.ModelCheckpoint("model_checkpoint/module-6/model-time-series")])
print(model_1_history.history)
plot_history(model_1_history.history)

#       model-2: fine-tuned feature extractor model for 5 epochs

base_model.trainable = False
for layer in base_model.layers[-30:]:
    layer.trainable = True

inputs = layers.Input([224, 224, 3], name="input_layer")
x = augmentation_layer(inputs)
x = base_model(x)
x = layers.GlobalAveragePooling2D(name="g_avg_pool")(x)
output = layers.Dense(10, name="output")(x)

model_2 = tf.keras.Model(inputs, output)

model_2.compile(loss=losses.CategoricalCrossentropy(),
                optimizer=optimizers.Adam(),
                metrics=["accuracy"])

print(model_2.summary())
model_2_history = model_2.fit(train_data,
                              epochs=5,
                              validation_data=test_data,
                              validation_steps=int(len(test_data) + 0.2))

print(model_1_history.history)
#       plot history graph
plot_history(model_2_history.history)



