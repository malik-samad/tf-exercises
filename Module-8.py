import tensorflow as tf
from tensorflow.keras import layers, Sequential,losses, optimizers
from tensorflow.keras.utils import plot_model
import matplotlib.pyplot as plt
import pandas as pd
import random
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization
import numpy as np
import tensorflow_hub as hub
from sklearn.metrics import confusion_matrix
import itertools

tf.random.set_seed(42)

# 1.    Rebuild, compile and train model_1, model_2 and model_5 using the Keras Sequential API
#       instead of the Functional API.
train_df = pd.read_csv("Data/nlp_getting_started/train.csv")
test_df = pd.read_csv("Data/nlp_getting_started/test.csv")
print(train_df.head(1))

x_train, x_test, y_train, y_test = train_test_split(train_df["text"],
                                                    train_df["target"],
                                                    test_size=0.1,
                                                    shuffle=True,
                                                    random_state=42)

print(len(x_train),x_train.shape, len(x_test), len(y_train), len(y_test), y_test[:10], x_train[:10])
max_vocab_length = 1000
max_length = 15

text_vectorizer = TextVectorization(max_tokens=max_vocab_length,
                                    output_sequence_length=max_length,
                                    output_mode="int")

text_vectorizer.adapt(np.array(x_train))
sentance = random.choice(np.array(x_train))
# print(sentance)
# print(text_vectorizer([sentance]))
# print(text_vectorizer.get_vocabulary()[:10])

# model_1 using sequential API
embedding_layer = layers.Embedding(input_dim=max_vocab_length,
                                   output_dim=128,
                                   input_length=max_length,
                                   name="embedding_layer")
model_1 = Sequential([
    layers.InputLayer(input_shape=(1,), dtype="string"),
    text_vectorizer,
    embedding_layer,
    layers.GlobalMaxPool1D(),
    layers.Dense(1, activation="sigmoid")
])

model_1.compile(loss=losses.binary_crossentropy,
                optimizer=optimizers.Adam(),
                metrics=["accuracy"])
# print(model_1.summary())
# plot_model(model_1, to_file="model_1.png")

model_1_history = model_1.fit(x_train,
                              y_train,
                              epochs=5,
                              validation_data=(x_test, y_test))

# model_2 using sequential API
embedding_layer = layers.Embedding(input_dim=max_vocab_length,
                                   output_dim=128,
                                   input_length=15)

model_2 = Sequential()
model_2.add(layers.InputLayer(input_shape=(1,), dtype="string", name="input_layer"))
model_2.add(text_vectorizer)
model_2.add(embedding_layer)
model_2.add(layers.LSTM(64))
model_2.add(layers.Dense(1, name="output", activation="sigmoid"))

model_2.compile(loss=losses.binary_crossentropy,
                optimizer=optimizers.Adam(),
                metrics=["accuracy"])

# loss: 0.3428 - accuracy: 0.8545 - val_loss: 0.5906 - val_accuracy: 0.7638
model_2_history = model_2.fit(x_train,
                              y_train,
                              epochs=5,
                              validation_data=(x_test, y_test))

# model_5 using sequential API
embedding_layer = layers.Embedding(input_dim=max_vocab_length,
                                   output_dim=128,
                                   input_length=max_length,
                                   name="embedding_layer")

model_5 = Sequential()
model_5.add(layers.InputLayer(input_shape=(1,), dtype="string", name="input_layer"))
model_5.add(text_vectorizer)
model_5.add(embedding_layer)
model_5.add(layers.Conv1D(filters=64, kernel_size=5, activation="relu", name="conv_64_1d"))
model_5.add(layers.GlobalMaxPool1D())
model_5.add(layers.Dense(1, activation="sigmoid", name="output_layer"))

model_5.compile(loss=losses.binary_crossentropy,
                optimizer=optimizers.Adam(),
                metrics=["accuracy"])

# loss: 0.5251 - accuracy: 0.7346 - val_loss: 0.5766 - val_accuracy: 0.7015
model_5_history = model_5.fit(x_train,
                              y_train,
                              epochs=5,
                              validation_data=(x_test, y_test))


# 2.    Try ne-tuning the TF Hub Universal Sentence Encoder model by setting training=True
#       when instantiating it as a Keras lay
sentence_encoder_layer = hub.KerasLayer("https://tfhub.dev/google/universal-sentence-encoder/4",
                                        input_shape=[],
                                        dtype=tf.string,
                                        trainable=True,
                                        name="USE")

# model_6 = tf.keras.Sequential([
#     layers.InputLayer(input_shape=(), dtype="string", name="input_layer"),
#     sentence_encoder_layer, # take in sentences and then encode them into an embedding
#     layers.Dense(64, activation="relu"),
#     layers.Dense(1, activation="sigmoid")
# ], name="model_6_USE")

inputs = layers.Input(shape=(), dtype='string', name="input_layer")
x = sentence_encoder_layer(inputs)
x = layers.Dense(64, activation="relu")(x)
output = layers.Dense(1, activation="sigmoid")(x)

model_6 = tf.keras.Model(inputs, output)

# Compile model
model_6.compile(loss="binary_crossentropy",
                optimizer=tf.keras.optimizers.Adam(),
                metrics=["accuracy"])

model_6.summary()

# loss: 0.0310 - accuracy: 0.9889 - val_loss: 0.6172 - val_accuracy: 0.8097
# model_6_history = model_6.fit(x_train,
#                               y_train,
#                               epochs=5,
#                               validation_data=(x_test, y_test))

# 3.    Retrain the best model you've got so far on the whole training set (no validation split). Then
#       use this trained model to make predictions on the test dataset and format the predictions
#       into the same format as the sample_submission.csv le from Kaggle (see the Files tab in
#       Colab for what the sample_submission.csv le looks like). Once you've done this, make a
#       submission to the Kaggle competition.


# 4.    Combine the ensemble predictions using the majority vote (mode), how does this perform
#       compare to averaging the prediction probabilities of each model
ensemble_model = [model_1, model_2, model_5]

def ensemble_pred (ensemble_model, x_data):
    ensemble_preds_mult = []
    for model in ensemble_model:
        pred_probs = model.predict(x_data)
        ensemble_preds_mult.append(np.squeeze(np.round_(pred_probs)))

    ensemble_preds_mult_transposed = np.transpose(np.array(ensemble_preds_mult))
    ensemble_preds = []
    for i in tf.range(0, len(ensemble_preds_mult_transposed)):
        unique, unique_count = np.unique(ensemble_preds_mult_transposed[i], return_counts=True)
        # print(f"inner Loop: {unique} - {np.argmax(unique_count)} - {unique_count}")
        ensemble_preds.append(unique[np.argmax(unique_count)])

    return tf.cast(ensemble_preds, dtype=tf.int8)

ensemble_preds = ensemble_pred(ensemble_model, x_test)
# print("ensemble Preds: ", ensemble_preds, y_test)

# 5.    Make a confusion matrix with the best performing model's predictions on the validation set
#       and the validation ground truth label

# Confusion matrix visualization
def visualize_confusion_matrix(y_true, y_preds, classes=None,  figsize=(10, 10), text_size=15):
    cm = confusion_matrix(y_true, y_preds)
    cm_norm = cm / np.sum(cm, axis=1)
    n_classes = cm.shape[0]

    fig, ax = plt.subplots(figsize=figsize)
    cax = ax.matshow(cm, cmap=plt.cm.Blues)
    fig.colorbar(cax)

    if classes:
        labels = classes
    else:
        labels = np.arange(cm.shape[0])

    print("\narange: ", np.arange(n_classes))

    ax.set(
        title="confusion matrix",
        xlabel="prediction",
        ylabel="true label",
        xticks=np.arange(n_classes),
        yticks=np.arange(n_classes),
        xticklabels=labels,
        yticklabels=labels,
    )

    ax.xaxis.set_label_position("bottom")
    ax.xaxis.tick_bottom()

    threshold = cm.max() + cm.min() / 2.0

    for i, j in itertools.product(np.arange(cm.shape[0]), np.arange(cm.shape[1])):
        plt.text(i, j, f"{cm[i, j]} ({cm_norm[i, j] * 100:.1f}%)",
                 horizontalalignment='center',
                 size=text_size)

    plt.show()


visualize_confusion_matrix(y_test, ensemble_preds, ["Disaster", "Non-Disaster"], figsize=(6, 4), text_size=9)
