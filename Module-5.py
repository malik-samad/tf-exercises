import tensorflow as tf
from tensorflow.keras import layers, losses, optimizers
from tensorflow.keras.preprocessing import image_dataset_from_directory
from tensorflow.keras.layers.experimental import preprocessing
import os
import matplotlib.pyplot as plt
import random

# dir paths
train_dir = "Data/10_food_classes_10_percent/train"
test_dir = "Data/10_food_classes_10_percent/test"

# 1. Write a function to visualize an image from any dataset (train or test file)
#    and any class (e.g. "steak", "pizza"... etc), visualize it and make a prediction on it using a trained model.


# img preprocessing layer
preprocessing_layer = tf.keras.Sequential([
    preprocessing.RandomFlip("horizontal"),
    preprocessing.RandomZoom(0.2),
    preprocessing.RandomRotation(0.2),
    # preprocessing.RandomHeight(0.2),
    # preprocessing.RandomWidth(0.2),
    # preprocessing.Rescaling(1/255.0)
])

def visualize_random_img(dir):
    all_dir = os.listdir(dir)
    random_dir = random.choice(all_dir)
    # print(dir, random_dir, all_dir)

    all_files = os.listdir(f"{dir}/{random_dir}")
    random_file = random.choice(all_files)
    # print(f"{dir}/{random_dir}/{random_file}")
    random_file = plt.imread(f"{dir}/{random_dir}/{random_file}")
    print("first: ", random_file.shape)

    plt.figure()
    plt.imshow(random_file)
    plt.title(random_dir)

    # random_file = preprocessing_layer(tf.expand_dims(random_file, axis=0))
    # print("second: ", tf.squeeze(random_file).shape)
    #
    # plt.figure()
    # plt.imshow(tf.squeeze(random_file))
    # plt.title(random_dir)
    # plt.show()

# visualize_random_img(train_dir)

# 2. Use feature-extraction to train a transfer learning model on 10% of the Food Vision data
#    for 10 epochs using tf.keras.applications.EfficientNetB0 as the base model.
#    Use the ModelCheckpoint callback to save the weights to file.

train_data = image_dataset_from_directory(train_dir,
                                          label_mode="categorical",
                                          image_size=[224, 224],
                                          shuffle=True,
                                          batch_size=32).prefetch(tf.data.AUTOTUNE)

test_data = image_dataset_from_directory(test_dir,
                                         image_size=[224, 224],
                                         batch_size=32,
                                         shuffle=True,
                                         label_mode="categorical").prefetch(tf.data.AUTOTUNE)

base_model = tf.keras.applications.EfficientNetB0(include_top=False)
base_model.trainable = True

for layer in base_model.layers[:-10]:
    layer.trainable = False


# print base_model layers for trainable flag
# for index, layer in enumerate(base_model.layers):
#     print(index, layer.trainable, layer.name)


inputs = layers.Input([224, 224, 3], name="input_layer")

# x = preprocessing_layer(inputs)
# print("preprocessing_layer: ", x)
print("inputs: ", inputs)
x = base_model(inputs)
print("base_model: ", x)
x = layers.GlobalAveragePooling2D(name="average_pool_2d")(x)
print("GlobalAveragePooling2D: ", x)
output = layers.Dense(10, activation="softmax")(x)
print("output: ", output)

model_1 = tf.keras.Model(inputs, output)

model_1.compile(loss=losses.CategoricalCrossentropy(),
                optimizer=optimizers.Adam(),
                metrics=["accuracy"])

# print(model_1.input_shape, train_data, train_data.take(1))
initial_epochs = 10
model_1_history = model_1.fit(train_data,
                              epochs=initial_epochs,
                              steps_per_epoch=len(train_data),
                              validation_data=test_data,
                              validation_steps=int(len(test_data) * 0.2),
                              callbacks=[tf.keras.callbacks.ModelCheckpoint("model-checkpoint/module-5/model-time-series",
                                                                            "val_accuracy",
                                                                            save_best_only=True)]
                              )
# model_1.evaluate(test_data)
model_1.load_weights('model-checkpoint/module-5/model-time-series')
model_1.evaluate(test_data)


# 3. Fine-tune the last 20 layers of the base model you trained in 2 for another 10 epochs. How did it go?

print(model_1.summary())

base_model.trainable = False

for layer in model_1.layers[1].layers[-20:]:
    print(layer.trainable, layer.name)
    layer.trainable = True

model_1.compile(loss=losses.CategoricalCrossentropy(),
                optimizer=optimizers.Adam(),
                metrics=["accuracy"])

fine_tune_epochs = initial_epochs + 10
model_1_history = model_1.fit(train_data,
                              epochs=fine_tune_epochs,
                              initial_epoch=initial_epochs,
                              steps_per_epoch=len(train_data),
                              validation_data=test_data,
                              validation_steps=int(len(test_data) * 0.2),
                              callbacks=[tf.keras.callbacks.ModelCheckpoint("model-checkpoint/module-5/model-time-series",
                                                                            "val_accuracy",
                                                                            save_best_only=True)]
                              )

for layer in model_1.layers[1].layers[-30:]:
    print(layer.trainable, layer.name)

print(model_1.summary())

# 4. Fine-tune the last 30 layers of the base model you trained in 2 for another 10 epochs. How did it go?
base_model.trainable = False

for layer in model_1.layers[1].layers[-30:]:
    print(layer.trainable, layer.name)
    layer.trainable = True

model_1.compile(loss=losses.CategoricalCrossentropy(),
                optimizer=optimizers.Adam(),
                metrics=["accuracy"])

fine_tune_epochs = fine_tune_epochs+10
model_1_history = model_1.fit(train_data,
                              epochs=fine_tune_epochs,
                              initial_epoch=model_1_history.epoch[-1],
                              steps_per_epoch=len(train_data),
                              validation_data=test_data,
                              validation_steps=int(len(test_data) * 0.2),
                              callbacks=[tf.keras.callbacks.ModelCheckpoint("model-checkpoint/module-5/model-time-series",
                                                                            "val_accuracy",
                                                                            save_best_only=True)]
                              )

for layer in model_1.layers[1].layers[-40:]:
    print(layer.trainable, layer.name)

print(model_1.summary())


# to do model fine-tune we need to follow these steps:
# 1. we have to use model checkpointing.
# 2. we will need to created a separate model same used for checkpointing.
# 3. make fine-tune (enable the layers to be trainable) and then fit the model

