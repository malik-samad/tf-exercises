from sklearn.datasets import make_circles, make_moons
from tensorflow.keras import layers
from tensorflow.keras import Sequential
import tensorflow as tf
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.metrics import confusion_matrix
import itertools
import matplotlib.pyplot as plt

run_test = 4
# Replicate the model pictured in the TensorFlow Playground diagram using TensorFlow code.
#   Compile it using the Adam optimizer, binary crossentropy loss and accuracy metric.
#   Once it's compiled check a summary of the model. tensorflow playground example neural network Try
#   this network out for yourself on the TensorFlow Playground website.
#   Hint: there are 5 hidden layers but the output layer isn't pictured,
#       you'll have to decide what the output layer should be based on the input data.
n_samples = 1000
if run_test == 1:
    x, y = make_circles(n_samples, random_state=42)
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)
    print(x_train.shape, y_train.shape, x_train[0], y_train[0], x_train[0].shape)
    model_1 = Sequential([
        layers.InputLayer(input_shape=(2), name="input-layer"),
        layers.Dense(6, activation="relu"),
        layers.Dense(6, activation="relu"),
        layers.Dense(6, activation="relu"),
        layers.Dense(6, activation="relu"),
        layers.Dense(6, activation="relu"),
        layers.Dense(1, activation="sigmoid")
    ])

    model_1.compile(loss=tf.keras.losses.BinaryCrossentropy(),
                    optimizer=tf.keras.optimizers.SGD(learning_rate=0.001),
                    metrics=['accuracy']
                    )

    model_1.fit(x_train, y_train,
                epochs=200,
                verbose=0,
                validation_data=(x_test, y_test))

    model_1.evaluate(x_test, y_test)
    print(model_1.input_shape, x_test[0], x_test[0].shape, (tf.expand_dims(x_test[0], axis=0)).shape)
    print(f"Predict:\nx: {x_test[0]}, pred:{model_1.predict(tf.expand_dims(x_test[0], axis=0))}, y_true: {y_test[0]}")
#
# # Create a classification dataset using Scikit-Learn's make_moons() function, visualize it and
# #   then build a model to fit it at over 85% accuracy.
if run_test == 2:
    x, y = make_moons(n_samples, random_state=42)
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=42)
    # visualize shapes and samples
    print(x_train.shape, y_train.shape, x_train[0], y_train[0])

    # visualize data in plt
    x1, y1 = np.transpose(x_train)[0], np.transpose(x_train)[1]
    print(x1.shape, y1.shape)
    plt.scatter(x1, y1)
    x1, y1 = np.transpose(x_test)[0], np.transpose(x_test)[1]
    plt.scatter(x1, y1)
    plt.show()

    # create model for it
    model_2 = Sequential([
        layers.InputLayer(input_shape=(2), name="input-layer"),
        layers.Dense(16, activation="relu"),
        layers.Dense(32, activation="relu"),
        layers.Dense(16, activation="relu"),
        layers.Dense(1, activation="sigmoid")
    ])

    model_2.compile(loss=tf.keras.losses.BinaryCrossentropy(),
                    optimizer=tf.keras.optimizers.Adam(learning_rate=0.01),
                    metrics=["accuracy"])

    model_2.fit(x_train,
                y_train,
                epochs=10,
                verbose=0,
                validation_data=(x_test, y_test))

    print(model_2.summary())

    # model_2 predictions
    print(
        f"model.shape : {model_2.input_shape}, test.shape: {x_test.shape}, sample.shape: {x_test[0].shape}, {tf.expand_dims(x_test[0], axis=0).shape}")
    print(
        f"Preds:\ninput: {tf.expand_dims(x_test[0], axis=0)}, prediction: {model_2.predict(tf.expand_dims(x_test[0], axis=0))}, y_true: {y_test[0]}")

    # make full preds
    preds = model_2.predict(x_test)
    print(f"preds: {preds.shape}, {tf.squeeze(preds).shape}, {tf.squeeze(preds)[0]}")
    x_0 = []
    x_1 = []
    for i, item in enumerate(preds):
        if item[0] < 0.5:
            x_0.append(x_test[i])
        else:
            x_1.append(x_test[i])

    # plot scatter for both
    x_trans = np.transpose(x_0)
    plt.scatter(x_trans[0], x_trans[1])
    x_trans = np.transpose(x_1)
    plt.scatter(x_trans[0], x_trans[1])
    plt.show()

# Create a function (or write code) to visualize multiple image predictions for the fashion MNIST at the same time.
#   Plot at least three different images and their prediction labels at the same time. Hint: see the classifcation
#   tutorial in the TensorFlow documentation for ideas.

class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']


def plot_imgs(train_images, train_labels):
    plt.figure(figsize=(7, 10))
    for i in range(4):
        plt.subplot(2, 2, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.grid(False)
        plt.imshow(train_images[i])
        plt.xlabel(f"actual:{class_names[train_labels[i]]}\npred:")
    plt.show()


# Recreate TensorFlow's softmax activation function in your own code. Make sure it can accept a tensor
#   and return that tensor after having the softmax function applied to it.
def custom_softmax(x, axis=0):
    # exp(x) / tf.reduce_sum(exp(x))
    e = np.exp(x - tf.reduce_max(x, axis=axis, keepdims=True))
    s = tf.reduce_sum(e, axis=axis, keepdims=True)
    print(
        f"A:{tf.reduce_max(x, axis=axis, keepdims=True)},\nB:{x - tf.reduce_max(x, axis=axis, keepdims=True)},\ne:{e},\ns:{s}")
    return e / s


# Confusion matrix visualization
def visualize_confusionMatrix(y_true, y_preds, classes=None,  figsize=(10, 10), text_size=15):
    cm = confusion_matrix(y_true, y_preds)
    cm_norm = cm / np.sum(cm, axis=1)
    n_classes = cm.shape[0]

    fig, ax = plt.subplots(figsize=figsize)
    cax = ax.matshow(cm, cmap=plt.cm.Blues)
    fig.colorbar(cax)

    if classes:
        labels = classes
    else:
        labels = np.arange(cm.shape[0])

    print("\narange: ", np.arange(n_classes))

    ax.set(
        title="confusion matrix",
        xlabel="prediction",
        ylabel="true label",
        xticks=np.arange(n_classes),
        yticks=np.arange(n_classes),
        xticklabels=labels,
        yticklabels=labels,
    )

    ax.xaxis.set_label_position("bottom")
    ax.xaxis.tick_bottom()

    threshold = cm.max() + cm.min() / 2.0

    for i, j in itertools.product(np.arange(cm.shape[0]), np.arange(cm.shape[1])):
        plt.text(i, j, f"{cm[i, j]} ({cm_norm[i, j] * 100:.1f}%)",
                 horizontalalignment='center',
                 size=text_size)

    plt.show()


# testing custom softmax function
# arr = tf.constant([[0.0, 0.1], [0.8, 0.2], [0.1, 0.4], [0.5, 0.0], [1.5, 0.5]])
# print(f"\ncustomSoftMax: {softmax(arr, axis=1)},\ntf.softMax: {tf.keras.activations.softmax(arr,axis=1)}")
fashion_mnist = tf.keras.datasets.fashion_mnist
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()
# train_labels = tf.one_hot(train_labels, depth=len(class_names), dtype=tf.float32)
# test_labels = tf.one_hot(test_labels, depth=len(class_names), dtype=tf.float32)
train_images = train_images/255.0
test_images = test_images/255.0
# print(f"\n{train_labels[:4]}, \n{test_labels[:4]}, \n{train_images[1]}")

# train dataset
train_images_dataset = tf.data.Dataset.from_tensor_slices(tf.cast(train_images, dtype=tf.float32))
train_labels_dataset = tf.data.Dataset.from_tensor_slices(tf.cast(train_labels, dtype=tf.float32))
train_dataset = tf.data.Dataset.zip((train_images_dataset, train_labels_dataset)) \
    .prefetch(tf.data.AUTOTUNE) \
    .shuffle(buffer_size=2000) \
    .batch(32)

# test dataset
test_images_dataset = tf.data.Dataset.from_tensor_slices(tf.cast(test_images, dtype=tf.float32))
test_labels_dataset = tf.data.Dataset.from_tensor_slices(tf.cast(test_labels, dtype=tf.float32))
test_dataset = tf.data.Dataset.zip((test_images_dataset, test_labels_dataset)) \
    .prefetch(tf.data.AUTOTUNE) \
    .shuffle(buffer_size=2000) \
    .batch(32)

# plot_imgs(train_images, train_labels)

if run_test == 3:
    tf.random.set_seed(42)
    # Train a model to get 88%+ accuracy on the fashion MNIST test set.
    # Plot a confusion matrix to see the results after.
    model_3 = Sequential([
        layers.Flatten(input_shape=(28, 28)),
        layers.Dense(256, activation="relu"),
        layers.Dense(128, activation="relu"),
        layers.Dense(128, activation="relu"),
        layers.Dense(len(class_names), activation="softmax"),
    ])

    model_3.compile(loss=tf.keras.losses.SparseCategoricalCrossentropy(),
                    optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001),
                    metrics=['accuracy'],)

    print(f"\n\nshapes: {model_3.input_shape},  {model_3.output_shape},"
          f"\n {train_images.shape}, {train_labels.shape},"
          f"\n {test_images.shape}, {test_labels.shape}"
          )
    # print(model_3.summary())
    model_3.fit(train_dataset,
                epochs=50,
                validation_data=test_dataset,
                callbacks=[tf.keras.callbacks.
                ModelCheckpoint("D:\DeepLearning\ZeroToMasteryExercises/model_checkpoint/model_3",
                                "val_accuracy",
                                save_best_only=True)])

    model_3.evaluate(test_dataset)


model_3 = tf.keras.models.load_model("D:\DeepLearning\ZeroToMasteryExercises/model_checkpoint/model_3")
history = model_3.evaluate(test_dataset)
print(f"\nhistory: {history}")

probs = model_3.predict(test_images)
print(probs[:4])

preds = probs.argmax(axis=1)
print(preds[:4])

visualize_confusionMatrix(y_true=test_labels, y_preds=preds, classes=class_names, text_size=5)

# Make a function to show an image of a certain class of the fashion MNIST dataset and make a prediction on it.
#   For example, plot 3 images of the T-shirt class with their predictions.
