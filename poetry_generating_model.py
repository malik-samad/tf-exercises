# -*- coding: utf-8 -*-

import tensorflow as tf
import numpy as np
import os
from tensorflow.keras import layers
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
# import zipfile

data_dir = "C:/Users/developer/Downloads/Compressed/Datasets/Poems dataset/"
# with zipfile.ZipFile("Downloads/Compressed/Datasets/Poems dataset.zip") as zip:
#   zip.extractall(data_dir)


data_dir = "C:/Users/developer/Downloads/Compressed/Datasets/Poems dataset/topics/"
def load_all_txt(data_dir):
  lyrics = np.array([], dtype=object)
  for sub_cat in os.listdir(data_dir): 
    for file_path in os.listdir(f"{data_dir}{sub_cat}"): 
      try:
        with open(f"{data_dir}{sub_cat}/{file_path}", encoding='utf8', mode="r") as f: 
          # lines = f.readlines() 
          lyrics = np.append(lyrics, f.readlines() )
      except:
        print(f"An exception occurred on ready file: {data_dir}{sub_cat}/{file_path}", )

  return lyrics

songs_lyrics = load_all_txt(data_dir)

songs_lyrics[:2]

sentences = np.squeeze(songs_lyrics)
sentences[:5], len(sentences)

word_count = 3000

tokenizer = Tokenizer(num_words=word_count)
tokenizer.fit_on_texts(sentences)
word_index = tokenizer.word_index
total_word_count = len(word_index) + 1

print("len(word_index)", len(word_index))

sequences = tokenizer.texts_to_sequences(sentences)
print("sequence len: ", len(sequences), sequences)

def create_sequences(sentences):
  processed = []
  for line in sentences:
    seq = tokenizer.texts_to_sequences([line])[0]
    for i in range(1, len(seq)):
      n_gram = seq[:i+1]
      processed.append(n_gram)
  return processed

processed_seq = create_sequences(sentences)

print(f"processed_seq: \n{processed_seq[:5]}\nsequences: {sentences[:5]} \n {len(processed_seq)}\n {len(sentences)}")

tokenizer.sequences_to_texts([[70]])

# pad seq
seq_len = [len(item) for item in processed_seq]
sent_len = [len(item) for item in sentences]

max_seq_len = 25 # int(np.percentile(sent_len, 95))
max_seq_len_1 = np.argmax(seq_len)
max_sent_len = np.argmax(sent_len)

print(max_seq_len, seq_len[:2], sentences[max_sent_len],  processed_seq[max_seq_len_1])

padded_input_seq = pad_sequences(processed_seq, max_seq_len, padding="pre", truncating="pre")
padded_input_seq.shape

x_data = padded_input_seq[:, :-1]
y_data = padded_input_seq[:, -1]
print(f"x_data: {x_data[:4]}\n y_data: {y_data[:4]}")

labels_one_hot = tf.one_hot(y_data, word_count, dtype=tf.int16)
print("labels_one_hot[1]: ", labels_one_hot[1])

x_data.shape, labels_one_hot.shape


model = tf.keras.Sequential([
    layers.Embedding(word_count, 256),
    tf.keras.layers.Conv1D(filters=32, kernel_size=3,
                            strides=1, padding="causal",
                            activation="relu", ),
    # layers.Lambda(lambda item : tf.transpose(item)),
    layers.Bidirectional(layers.LSTM(32, return_sequences=True)),
    layers.Bidirectional(layers.LSTM(32)),
    # layers.Dense(500, activation="relu"),
    layers.Dense(word_count, name="output_layer", activation="softmax")
])

model.compile(loss=tf.keras.losses.categorical_crossentropy,
              optimizer=tf.keras.optimizers.Adam(learning_rate=0.01),
              metrics=["accuracy"])

model.summary()

for layer in model.layers:
    print(layer.name, layer.input_shape, layer.output_shape)

# OOM error occurs when creating big dataset
# dataset = tf.data.Dataset.from_tensor_slices((x_data, labels_one_hot)).shuffle(True).batch(64).prefetch(tf.data.AUTOTUNE)
# print(dataset)

model.fit(x_data, labels_one_hot,
          epochs=100,
          batch_size=1000,
          callbacks=[tf.keras.callbacks.ModelCheckpoint(f"C:/Users/developer/Downloads/Compressed/Datasets/model-checkpoints/one", monitor="loss"),
                     tf.keras.callbacks.EarlyStopping(monitor="loss", patience=5)])


