import tensorflow as tf
from tensorflow.keras import layers
import numpy as np
import os
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

word_count = 3000
max_seq_len = 25

model = tf.keras.Sequential([
    layers.Embedding(word_count, 256),
    tf.keras.layers.Conv1D(filters=32, kernel_size=3,
                            strides=1, padding="causal",
                            activation="relu", ),
    # layers.Lambda(lambda item : tf.transpose(item)),
    layers.Bidirectional(layers.LSTM(32, return_sequences=True)),
    layers.Bidirectional(layers.LSTM(32)),
    # layers.Dense(500, activation="relu"),
    layers.Dense(word_count, name="output_layer", activation="softmax")
])

model.compile(loss=tf.keras.losses.categorical_crossentropy,
              optimizer=tf.keras.optimizers.Adam(learning_rate=0.01),
              metrics=["accuracy"])

model.load_weights("C:/Users/developer/Downloads/Compressed/Datasets/model-checkpoints/one")

data_dir = "C:/Users/developer/Downloads/Compressed/Datasets/Poems dataset/topics/"


def load_all_txt(data_dir):
  lyrics = np.array([], dtype=object)
  for sub_cat in os.listdir(data_dir):
    for file_path in os.listdir(f"{data_dir}{sub_cat}"):
      try:
        with open(f"{data_dir}{sub_cat}/{file_path}", encoding='utf8', mode="r") as f:
          # lines = f.readlines()
          lyrics = np.append(lyrics, f.readlines() )
      except:
        print(f"An exception occurred on reading file: {data_dir}{sub_cat}/{file_path}", )

  return lyrics

songs_lyrics = load_all_txt(data_dir)

songs_lyrics[:2]

sentences = np.squeeze(songs_lyrics)
sentences[:5], len(sentences)

word_count = 3000

tokenizer = Tokenizer(num_words=word_count)
tokenizer.fit_on_texts(sentences)
word_index = tokenizer.word_index
total_word_count = len(word_index) + 1

print("len(word_index)", len(word_index))

sequences = tokenizer.texts_to_sequences(sentences)
print("sequence len: ", len(sequences), sequences)


def generate_lyrics(model, input, length=10):
  for i in range(length):
    # tokenize
    input_seq = tokenizer.texts_to_sequences([input])
    print("input_seq: ", input_seq)
    # padd
    padded_input_seq = pad_sequences(input_seq, maxlen=max_seq_len)

    # predict
    pred = model.predict(padded_input_seq, verbose=0)
    print(pred.shape, pred[0])
    pred = np.argmax(pred[0])
    print(f"{i} - pred : {pred}")
    output_word = tokenizer.sequences_to_texts([[pred]])[0]
    print(f"output_word: {output_word}")
    # append to the input
    input += f" {output_word}"

  return input


print(generate_lyrics(model, "happy birthday", 50))
