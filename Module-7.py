import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras import layers, losses, optimizers
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from custom_save.module_7.model_1_history import history as model_1_history
import pandas as pd
import itertools as itertools
from sklearn.metrics import confusion_matrix, f1_score
import random
import os


# 1. Use the same evaluation techniques on the large-scale Food Vision model as you
#    did in the previous notebook (Transfer Learning Part 3: Scaling up). More specifically, it would be good to see:

# load in data
train_dir = "Data/10_food_classes_10_percent/train"
test_dir = "Data/10_food_classes_10_percent/test"

train_gen = ImageDataGenerator(rescale=1/255.)
test_gen = ImageDataGenerator(rescale=1/255.)

train_data = tf.keras.preprocessing.image_dataset_from_directory(train_dir,
                                                                 image_size=[224, 224],
                                                                 label_mode="categorical",
                                                                 batch_size=32,
                                                                 shuffle=True)
test_data = tf.keras.preprocessing.image_dataset_from_directory(test_dir,
                                                                image_size=[224, 224],
                                                                label_mode="categorical",
                                                                batch_size=32,
                                                                shuffle=True)

test_data_gen = test_gen.flow_from_directory(test_dir,
                                             target_size=[224, 224],
                                             class_mode="categorical",
                                             batch_size=32,
                                             shuffle=True)
class_names = ['apple_pie',
               'baby_back_ribs',
               'baklava',
               'beef_carpaccio',
               'beef_tartare',
               'beet_salad',
               'beignets',
               'bibimbap',
               'bread_pudding',
               'breakfast_burrito']
test_img = []
test_lbl = []
for image, label in test_data.unbatch():
    test_img.append(image)
    test_lbl.append(np.argmax(label))
test_lbl = np.array(test_lbl)

augmentation_layer = tf.keras.Sequential([
    layers.experimental.preprocessing.RandomFlip('horizontal'),
    layers.experimental.preprocessing.RandomZoom(0.2)
])


base_model = tf.keras.applications.EfficientNetB3(include_top=False)
base_model.trainable = False

for layer in base_model.layers[-20:]:
    layer.trainable = True

inputs = layers.Input(shape=[224, 224, 3], name="input_layer")
x = augmentation_layer(inputs)
x = base_model(x)
x = layers.GlobalAveragePooling2D(name="glob_avg_pool")(x)
output = layers.Dense(10, name="output_layer")(x)

model_1 = tf.keras.Model(inputs, output)

model_1.compile(loss=losses.CategoricalCrossentropy(),
                optimizer=optimizers.Adam(),
                metrics=["accuracy"])

# model_1_history = model_1.fit(train_data,
#                               epochs=10,
#                               # steps_per_epoch=int(len(test_data) * 0.1),
#                               validation_data=test_data,
#                               validation_steps=int(len(test_data) * 0.2),
#                               callbacks=[tf.keras.callbacks.ModelCheckpoint("model_checkpoint/module_7/model_1",
#                                                                             save_best_only=True,
#                                                                             monitor="val_accuracy")]
#                               )

model_1 = tf.keras.models.load_model("model_checkpoint/module_7/model_1")

# save_history_to_file = open("custom_save/module_7/model_1_history.py", "w")
# text_to_save = f"history = {{'loss': [{', '.join(str(i) for i in model_1_history.history['loss'])}]," \
#           f"\n           'val_loss': [{', '.join(str(i) for i in model_1_history.history['val_loss'])}]," \
#           f"\n           'accuracy': [{', '.join(str(i) for i in model_1_history.history['accuracy'])}]," \
#           f"\n           'val_accuracy': [{', '.join(str(i) for i in model_1_history.history['val_accuracy'])}]}} \n"
# save_history_to_file.write(text_to_save)

#    a. A confusion matrix between all of the model's predictions and true labels.

# Confusion matrix visualization
def visualize_confusion_matrix(y_true, y_preds, classes=None,  figsize=(10, 10), text_size=15):
    cm = confusion_matrix(y_true, y_preds)
    cm_norm = cm / np.sum(cm, axis=1)
    n_classes = cm.shape[0]

    fig, ax = plt.subplots(figsize=figsize)
    cax = ax.matshow(cm, cmap=plt.cm.Blues)
    fig.colorbar(cax)

    if classes:
        labels = classes
    else:
        labels = np.arange(cm.shape[0])

    print("\narange: ", np.arange(n_classes))

    ax.set(
        title="confusion matrix",
        xlabel="prediction",
        ylabel="true label",
        xticks=np.arange(n_classes),
        yticks=np.arange(n_classes),
        xticklabels=labels,
        yticklabels=labels,
    )

    ax.xaxis.set_label_position("bottom")
    ax.xaxis.tick_bottom()

    threshold = cm.max() + cm.min() / 2.0

    for i, j in itertools.product(np.arange(cm.shape[0]), np.arange(cm.shape[1])):
        plt.text(i, j, f"{cm[i, j]} ({cm_norm[i, j] * 100:.1f}%)",
                 horizontalalignment='center',
                 size=text_size)

    plt.show()


pred_probs = model_1.predict(test_data)
print(pred_probs.shape)
preds = np.argmax(pred_probs, axis=1)
print("preds[:10]: ", preds[:10], test_lbl[:10], preds.shape, test_lbl.shape)
print("confusion_matrix: ", confusion_matrix(np.squeeze(test_lbl), preds))
print("test_data: ", test_data.class_names)
visualize_confusion_matrix(test_lbl,
                           preds,
                           classes=test_data.class_names,
                           figsize=(6, 6),
                           text_size=4)

#    b. A graph showing the f1-scores of each class.
model_1_f1_score = f1_score(test_lbl, preds, average=None)
model_1_f1_score = pd.DataFrame(model_1_f1_score)
print("model_1_f1_score: ", model_1_f1_score)
model_1_f1_score.plot.bar()
plt.show()


#    c. A visualization of the model making predictions on various images and comparing
#       the predictions to the ground truth.
#       *  For example, plot a sample image from the test dataset and have the title of the plot show the prediction,
#          the prediction probability and the ground truth label.
def visualize_random_pred(model, path):
    label = random.choice(os.listdir(path))
    image = random.choice(os.listdir(f"{path}/{label}"))
    image = plt.imread(f"{path}/{label}/{image}")
    plt.imshow(image)

    image = tf.image.resize_with_crop_or_pad(image, 224, 224)


    print("visualize_random_pred: ", image.shape)
    pred_probs = model.predict(np.expand_dims(image, axis=0))
    preds = np.argmax(pred_probs, axis=1)
    print("visualize_random_pred: ", preds, pred_probs, label, class_names[preds[0]])

    plt.imshow(image)
    plt.title(f"Preds: {class_names[preds[0]]} / Actual: {label}")
    plt.axis(False)
    plt.show()


visualize_random_pred(model_1, "Data/10_food_classes_10_percent/test")

# 2. Take 3 of your own photos of food and use the Food Vision model to make predictions on them.

