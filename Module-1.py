import tensorflow as tf
import tensorflow.keras.layers as layers
import numpy as np
import pandas as pd
from tensorflow.keras import Sequential
from sklearn.model_selection import train_test_split


# 1. Create your own regression dataset (or make the one we created in "Create data to view and fit" bigger)
#    and build fit a model to it.
features = np.arange(start=1, stop=50, step=1)
Y = tf.cast(features * 2 + 5, dtype=tf.float32).numpy()
X = tf.cast(features * 1.9, dtype=tf.float32).numpy()

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=42)

X_train_dataset = tf.data.Dataset.from_tensor_slices(x_train)
Y_train_dataset = tf.data.Dataset.from_tensor_slices(y_train)
train_dataset = tf.data.Dataset.zip((X_train_dataset, Y_train_dataset)).prefetch(True).batch(10)

X_test_dataset = tf.data.Dataset.from_tensor_slices(x_test)
Y_test_dataset = tf.data.Dataset.from_tensor_slices(y_test)
test_dataset = tf.data.Dataset.zip((X_test_dataset, Y_test_dataset)).prefetch(True).batch(10)
print(train_dataset, "\n\n", test_dataset)

model_1 = Sequential([
    layers.Dense(10, activation="relu"),
    layers.Dense(1),     # output
])

model_1.compile(loss="mae",
                optimizer=tf.keras.optimizers.Adam(lr=0.01),
                metrics=["mae"])

model_1.fit(train_dataset,
            epochs=300,
            validation_data=test_dataset,
            verbose=0
            # validation_steps=len(test_dataset)
            )

model_1.evaluate(test_dataset)
print(f"input shape: {tf.constant([[x_test[0]]]).shape}")
print(f"model_shape: {model_1.input_shape}")
print(f"test_shape: {test_dataset}")
pred = model_1.predict(tf.constant([[x_test[0]]]))
print(f"model_1 \n\tfeature: {x_test[0]}, predict: {pred}, original: {y_test[0]}")

# 2. Try building a neural network with 4 Dense layers and fitting it to your own regression dataset,
#    how does it perform?

model_2 = Sequential([
    layers.Dense(10, activation="relu"),
    layers.Dense(10, activation="elu"),
    layers.Dense(10, activation="relu"),
    layers.Dense(1),     # output
])

model_2.compile(loss="mae",
                optimizer=tf.keras.optimizers.Adam(lr=0.01),
                metrics=["mae"])

model_2.fit(train_dataset,
            epochs=300,
            validation_data=test_dataset,
            verbose=0
            # validation_steps=len(test_dataset)
            )

model_2.evaluate(test_dataset)
pred = model_2.predict(tf.constant([[x_test[0]]]))
print(f"model_2 \n\tfeature: {x_test[0]}, predict: {pred}, original: {y_test[0]}")

# Try and improve the results we got on the insurance dataset
# * Building a larger model (how does one with 4 dense layers go?).

model_3 = Sequential([
    layers.Dense(10, activation="relu"),
    layers.Dense(20, activation="relu"),
    layers.Dense(30, activation="relu"),
    layers.Dense(30, activation="relu"),
    layers.Dense(20, activation="relu"),
    layers.Dense(10, activation="relu"),
    layers.Dense(1),     # output
])

model_3.compile(loss="mae",
                optimizer=tf.keras.optimizers.Adam(lr=0.01),
                metrics=["mae"])

model_3.fit(train_dataset,
            epochs=600,
            validation_data=test_dataset,
            verbose=0
            # validation_steps=len(test_dataset)
            )

model_3.evaluate(test_dataset)
pred = model_3.predict(tf.constant([[x_test[0]]]))
print(f"model_3 \n\tfeature: {x_test[0]}, predict: {pred}, original: {y_test[0]}")


# 4. Import the Boston pricing dataset from TensorFlow tf.keras.datasets and model it.
# The Boston house-price data of Harrison, D. and Rubinfeld, D.L. 'Hedonic
#  prices and the demand for clean air', J. Environ. Economics & Management,
#  vol.5, 81-102, 1978.   Used in Belsley, Kuh & Welsch, 'Regression diagnostics
#  ...', Wiley, 1980.   N.B. Various transformations are used in the table on
#  pages 244-261 of the latter.
#
#  Variables in order:
#  CRIM     per capita crime rate by town
#  ZN       proportion of residential land zoned for lots over 25,000 sq.ft.
#  INDUS    proportion of non-retail business acres per town
#  CHAS     Charles River dummy variable (= 1 if tract bounds river; 0 otherwise)
#  NOX      nitric oxides concentration (parts per 10 million)
#  RM       average number of rooms per dwelling
#  AGE      proportion of owner-occupied units built prior to 1940
#  DIS      weighted distances to five Boston employment centres
#  RAD      index of accessibility to radial highways
#  TAX      full-value property-tax rate per $10,000
#  PTRATIO  pupil-teacher ratio by town
#  B        1000(Bk - 0.63)^2 where Bk is the proportion of blacks by town
#  LSTAT    % lower status of the population
#  MEDV     Median value of owner-occupied homes in $1000's
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.boston_housing.load_data(
    path='boston_housing.npz', test_split=0.2, seed=113
)
print("Shapes: ", x_train.shape, y_train.shape, x_test.shape, y_test.shape)
print(f"Samples: \n{x_train[0]}\n{y_train[0]}\n{x_test[0]}\n{y_test[0]}")

model_4 = Sequential([
    layers.InputLayer(input_shape=(13), name="input-layer"),
    layers.Dense(20, activation='relu'),
    layers.Dense(1),
])

model_4.compile(loss='mae',
                optimizer=tf.keras.optimizers.Adam(lr=0.1))

print(model_4.summary())

model_4.fit(x_train,
            y_train,
            epochs=50,
            # steps_per_epoch=len(x_train),
            # batch_size=32,
            validation_data=(x_test, y_test),
            validation_steps=len(x_test),
            verbose=0
            )

model_4.evaluate(x_test, y_test)
