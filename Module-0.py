import tensorflow as tf
import math as math

# 1. Create
scalar = tf.constant(12)
vector = tf.constant([1, 4])
matrix = tf.constant([[1, 2],
                      [3, 4]])
tensor = tf.constant([[1, 2, 3],
                      [4, 5, 6],
                      [7, 8, 9],
                      [10, 11, 12]])

print(scalar, vector, matrix, tensor)


# Shape, Rank, Size
print(f'shape of tensor: {tensor.shape}')
print(f'size of tensor: {tf.size(tensor)}')
print(f'rank of tensor: {tensor.ndim}')


# Create 2 matrix of size([5,300]) with random values
matrix_1 = tf.random.Generator.from_seed(1)
matrix_2 = matrix_1.normal([5, 300])
matrix_1 = matrix_1.normal([5, 300])
matrix_2 = tf.abs(matrix_2.numpy() % 1)
matrix_1 = tf.abs(matrix_1.numpy() % 1)
print("modified-1: ", matrix_1)
print("modified-2: ", matrix_2)


# matrix multiplication
matrix_3 = tf.matmul(matrix_1, tf.transpose(matrix_2))
print("\n\n\tmatrix multiplication:\n", matrix_3)

# matrix dot product
matrix_3 = tf.tensordot(matrix_1, tf.transpose(matrix_2), axes=1)
print("\n\n\tmatrix dot prod:\n", matrix_3)

# min and max values in tensor
print(f"min value in tensor: {tf.reduce_min(matrix_3)}")
print(f"max value in tensor: {tf.reduce_max(matrix_3)}")

# reshape the matrix_3 to [1, none, none]
matrix_3 = tf.expand_dims(matrix_3, axis=0)
print(f"expand shape: {matrix_3.shape}")
print(f"squeeze shape: {tf.squeeze(matrix_3).shape}")


#  create tensor with shape [10] and find max
ten_val = tf.constant([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
print(f"max value: {tf.reduce_max(ten_val)}")


# one-hot encode
print(f"one hot encode: \n{tf.one_hot(ten_val, depth=10)}")
