# Spend 20-minutes reading and interacting with the CNN explainer website.
# *  What are the key terms? e.g. explain convolution in your own words, pooling in your own words

# Play around with the "understanding hyper-parameters" section in the CNN explainer website for 10-minutes.
# * What is the kernel size?
#       it is the size of the extracting features from the given data, for image it would be pixels data.
#       a kernel size of 2 will pick features from 2 rows and will contain 4 pixels.
# * What is the stride?
#       A stride is the no of pixel for each step.
#       A stride with value 1 will make the feature extractor to step forward by 1 pixel.
# * How could you adjust each of these in TensorFlow code?
#     tf.keras.layers.Conv2D(
#         filters, kernel_size, strides=(1, 1), padding='valid',
#         data_format=None, dilation_rate=(1, 1), groups=1, activation=None,
#         use_bias=True, kernel_initializer='glorot_uniform',
#         bias_initializer='zeros', kernel_regularizer=None,
#         bias_regularizer=None, activity_regularizer=None, kernel_constraint=None,
#         bias_constraint=None, **kwargs
#     )

# Find an ideal learning rate for a simple convolutional neural network model on your the 10 class dataset.
import tensorflow as tf
from tensorflow.keras import layers, Sequential
from tensorflow.keras.preprocessing.image import ImageDataGenerator

# train and test directories
train_dir = "Data/10_food_classes_all_data/train"
test_dir = "Data/10_food_classes_all_data/test"

# create ImageDataGenerators for train and test data
train_data_gen = ImageDataGenerator(rescale=1/255.)
test_data_gen = ImageDataGenerator(rescale=1/255.)

# turn it into batches
train_data = train_data_gen.flow_from_directory(directory=train_dir,
                                                target_size=(224, 224),
                                                class_mode='categorical',
                                                shuffle=True,
                                                batch_size=32)
test_data = train_data_gen.flow_from_directory(directory=test_dir,
                                               target_size=(224, 224),
                                               class_mode='categorical',
                                               shuffle=True,
                                               batch_size=32)

print(f"train_data.num_classes: {train_data.num_classes}")

# model_1 for 10 food classes
model_1 = Sequential([
    layers.Conv2D(10, kernel_size=3, input_shape=(224, 224, 3), activation="relu"),
    layers.Conv2D(10, kernel_size=3, activation="relu"),
    layers.MaxPool2D(),
    layers.Conv2D(10, kernel_size=3, activation="relu"),
    layers.Conv2D(10, kernel_size=3, activation="relu"),
    layers.MaxPool2D(),
    layers.Flatten(),
    layers.Dense(train_data.num_classes, name="output-layer", activation="softmax")
])

# compile the model
model_1.compile(loss=tf.keras.losses.CategoricalCrossentropy(),
                optimizer=tf.keras.optimizers.Adam(),
                metrics=["accuracy"])

# fit the model
model_1_history = model_1.fit(train_data,
                              epochs=5,
                              steps_per_epoch=len(train_data),
                              validation_data=test_data,
                              validation_steps=len(test_data))

# Before shuffle:
#     Epoch 1/5
#     235/235 [==============================] - 219s 928ms/step - loss: 2.1804 - accuracy: 0.1931
#     - val_loss: 2.1515 - val_accuracy: 0.2004
#     Epoch 2/5
#     235/235 [==============================] - 216s 921ms/step - loss: 1.9610 - accuracy: 0.3075
#     - val_loss: 1.8784 - val_accuracy: 0.3440
#     Epoch 3/5
#     235/235 [==============================] - 212s 902ms/step - loss: 1.6983 - accuracy: 0.4195
#     - val_loss: 1.8822 - val_accuracy: 0.3516
#     Epoch 4/5
#     235/235 [==============================] - 210s 893ms/step - loss: 1.2116 - accuracy: 0.6003
#     - val_loss: 2.1212 - val_accuracy: 0.3032
#     Epoch 5/5
#     235/235 [==============================] - 230s 981ms/step - loss: 0.5857 - accuracy: 0.8191
#     - val_loss: 2.9117 - val_accuracy: 0.2812


# After shuffle:
#     Epoch 1/5
#     235/235 [==============================] - 234s 993ms/step - loss: 2.1529 - accuracy: 0.2065
#     - val_loss: 1.9891 - val_accuracy: 0.2852
#     Epoch 2/5
#     235/235 [==============================] - 230s 980ms/step - loss: 1.8583 - accuracy: 0.3569
#     - val_loss: 1.9072 - val_accuracy: 0.3164
#     Epoch 3/5
#     235/235 [==============================] - 258s 1s/step - loss: 1.4478 - accuracy: 0.5151
#     - val_loss: 2.0160 - val_accuracy: 0.3204
#     Epoch 4/5
#     235/235 [==============================] - 225s 957ms/step - loss: 0.8235 - accuracy: 0.7279
#     - val_loss: 2.5160 - val_accuracy: 0.2972
#     Epoch 5/5
#     235/235 [==============================] - 224s 952ms/step - loss: 0.3025 - accuracy: 0.9067
#     - val_loss: 3.8118 - val_accuracy: 0.2812

