import tensorflow as tf
import tensorflow_hub as hub
import numpy as np
from tensorflow.keras import layers, optimizers, losses, Sequential
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt
import cv2


# Plot loss curve
def plot_history_curve(history):
    loss = history["loss"]
    val_loss = history["val_loss"]

    accuracy = history["accuracy"]
    val_accuracy = history["val_accuracy"]

    epochs = range(len(loss))

    plt.plot(epochs, loss, label="loss")
    plt.plot(epochs, val_loss, label="val_loss")
    plt.title("Plot Loss")
    plt.show()

    plt.plot(epochs, accuracy, label="accuracy")
    plt.plot(epochs, val_accuracy, label="val_accuracy")
    plt.title("Plot Accuracy")
    plt.show()

# 1. Build and fit a model using the same data we have here but
#    with the MobileNetV2 architecture feature extraction (mobilenet_v2_100_224/feature_vector) from TensorFlow Hub,
#    how does it perform compared to our other models?
#    url: https://tfhub.dev/google/imagenet/mobilenet_v2_100_224/feature_vector/5
#    data: https://storage.googleapis.com/ztm_tf_course/food_vision/10_food_classes_10_percent.zip


test_dir = "Data/10_food_classes_10_percent/test"
train_dir = "Data/10_food_classes_10_percent/train"

test_gen = ImageDataGenerator(rescale=1/255.0)
train_gen = ImageDataGenerator(rescale=1/255.0)

test_data = test_gen.flow_from_directory(directory=test_dir,
                                         target_size=[224, 224],
                                         class_mode="categorical",
                                         shuffle=True,
                                         batch_size=32)
train_data = train_gen.flow_from_directory(directory=train_dir,
                                           target_size=[224, 224],
                                           class_mode="categorical",
                                           shuffle=True,
                                           batch_size=32)
num_classes = train_data.num_classes

model_1 = tf.keras.Sequential([
    hub.KerasLayer("https://tfhub.dev/google/imagenet/mobilenet_v2_100_224/feature_vector/5",
                   trainable=False),
    tf.keras.layers.Dense(num_classes, activation='softmax')
])

model_1.compile(loss='categorical_crossentropy',
                optimizer=tf.keras.optimizers.Adam(),
                metrics=["accuracy"])

model_1_history = model_1.fit(train_data,
                              epochs=3,
                              steps_per_epoch=len(train_data),
                              validation_data=test_data,
                              validation_steps=int(len(test_data) * 0.2))

model_1.evaluate(train_data)
print(model_1_history.history)

plot_history_curve(model_1_history.history)


# 2. Build a model to classify images of two different things you've taken photos of.
#       * You can use any feature extraction layer from TensorFlow Hub you like for this.
#       * You should aim to have at least 10 images of each class, for example to build
#         a fridge versus oven classifier, you'll want 10 images of fridges and 10 images of ovens.

train_dir = "Data/fridge_oven_10_pic"
test_dir = "Data/fridge_oven_10_pic"

classes = ["Fridge", "Oven"]
train_data = train_gen.flow_from_directory(train_dir,
                                           target_size=[224, 224],
                                           batch_size=32,
                                           shuffle=True,
                                           class_mode="categorical")
num_classes = train_data.num_classes
model_2 = tf.keras.Sequential([
    hub.KerasLayer("https://tfhub.dev/google/imagenet/mobilenet_v2_100_224/feature_vector/5",
                   trainable=False),
    tf.keras.layers.Dense(num_classes, activation='softmax')
])

model_2.compile(loss=losses.CategoricalCrossentropy(),
                optimizer=optimizers.Adam(),
                metrics=['accuracy'])

model_2.fit(train_data,
            epochs=4, )

model_2.evaluate(train_data)

# load image for test
img_path = "Data/eval_oven.jpg"
img_data = cv2.imread(img_path)
img_data_resized = cv2.resize(img_data, (224, 224))

# visualize
# cv2.imshow("before", img_data)
# cv2.imshow("resized", img_data_resized)
# cv2.waitKey(0)

img_data_resized = tf.cast(img_data_resized / 225.0, dtype=tf.float32)
img_data_resized = tf.expand_dims(img_data_resized, axis=0)

model_2_probs = model_2.predict(img_data_resized)
print(model_2_probs, np.argmax(model_2_probs), classes[np.argmax(model_2_probs)])

